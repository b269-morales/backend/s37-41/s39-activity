const Course = require("../models/Course.js");

const bcrypt = require("bcrypt");

const auth = require("../auth");


/*module.exports.addCourse = (reqBody, user) => {
	return new Promise((resolve, reject) => {
		if (!user.isAdmin) {
			reject("User needs to be admin to add course");
		} else {
			Course.find({ name: reqBody.name }).then((result) => {
				if (result.length > 0) {
					reject(`Course ${reqBody.name} is already registered`);
				} else {
					let newCourse = new Course({
						name: reqBody.name,
						description: reqBody.description,
						price: reqBody.price,
					});

					newCourse.save().then((course, error) => {
						if (error) {
							reject(false);
						} else {
							resolve(true);
						}
					});
				}
			});
		}
	});
};
*/
module.exports.addCourse = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});
		// Saves the created object to our database
		return newCourse.save().then((course, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});
	} 
	// User is not an admin
	let message = Promise.resolve(`User must be ADMIN to access this!`);
	return message.then((value) => {
		return {value}
	})
};